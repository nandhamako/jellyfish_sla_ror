# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_31_062157) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "business_hours", force: :cascade do |t|
    t.boolean "working_on_sunday"
    t.string "sun_st_time"
    t.string "sun_end_time"
    t.boolean "working_on_monday"
    t.string "mon_st_time"
    t.string "mon_end_time"
    t.boolean "working_on_tuesday"
    t.string "tue_st_time"
    t.string "tue_end_time"
    t.boolean "working_on_wednesday"
    t.string "wed_st_time"
    t.string "wed_end_time"
    t.boolean "working_on_thursday"
    t.string "thu_st_time"
    t.string "thu_end_time"
    t.boolean "working_on_friday"
    t.string "fri_st_time"
    t.string "fri_end_time"
    t.boolean "working_on_saturday"
    t.string "sat_st_time"
    t.string "sat_end_time"
    t.string "time_zone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "general_settings", force: :cascade do |t|
    t.string "freshdesk_api_key"
    t.string "freshdesk_domain"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "api_key"
  end

  create_table "holidays", force: :cascade do |t|
    t.date "date"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "priorities", force: :cascade do |t|
    t.string "name"
    t.integer "fd_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sla_settings", force: :cascade do |t|
    t.integer "status_id"
    t.integer "priority_id"
    t.integer "hours", default: 0
    t.integer "minutes", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name"
    t.integer "fd_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ticket_histories", force: :cascade do |t|
    t.integer "ticket_id"
    t.integer "status_id"
    t.datetime "in_time"
    t.datetime "out_time"
    t.float "time_spent", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.json "fd_data"
    t.integer "last_status_id"
    t.integer "current_status_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "full_name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
