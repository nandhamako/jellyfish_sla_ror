class CreateTicketHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :ticket_histories do |t|
      t.integer :ticket_id
      t.integer :status_id
      t.datetime :in_time
      t.datetime :out_time
      t.float :time_spent, default: 0
      t.timestamps
    end
  end
end
