class CreateBusinessHours < ActiveRecord::Migration[6.0]
  def change
    create_table :business_hours do |t|    
      t.boolean :working_on_sunday
      t.string :sun_st_time
      t.string :sun_end_time     
      t.boolean :working_on_monday
      t.string :mon_st_time
      t.string :mon_end_time
      t.boolean :working_on_tuesday  
      t.string :tue_st_time
      t.string :tue_end_time
      t.boolean :working_on_wednesday
      t.string :wed_st_time
      t.string :wed_end_time
      t.boolean :working_on_thursday
      t.string :thu_st_time
      t.string :thu_end_time
      t.boolean :working_on_friday
      t.string :fri_st_time
      t.string :fri_end_time
      t.boolean :working_on_saturday
      t.string :sat_st_time
      t.string :sat_end_time
      
      t.string :time_zone
      t.timestamps
    end
  end
end
