class CreateSlaSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :sla_settings do |t|
      t.integer :status_id
      t.integer :priority_id
      t.integer :hours, :default => 0
      t.integer :minutes, :default => 0

      t.timestamps
    end
  end
end
