class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.json :fd_data
      t.integer :last_status_id
      t.integer :current_status_id
      
      t.timestamps
    end
  end
end
