class AddApiKeyToGeneralSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :general_settings, :api_key, :text
  end
end
