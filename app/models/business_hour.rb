class BusinessHour < ApplicationRecord
  validates_presence_of :mon_st_time, :if =>:working_on_monday?, :message => "Start time and end time not present for monday"
  validates_presence_of :tue_st_time, :if =>:working_on_tuesday?, :message => "Start time and end time not present for tuesday"
  validates_presence_of :wed_st_time, :if =>:working_on_wednesday?, :message => "Start time and end time not present for wednesday"
  validates_presence_of :thu_st_time, :if =>:working_on_thursday?, :message => "Start time and end time not present for thursday"
  validates_presence_of :fri_st_time, :if =>:working_on_friday?, :message => "Start time and end time not present for friday"
  validates_presence_of :sat_st_time, :if =>:working_on_saturday?, :message => "Start time and end time not present for satuday"
  validates_presence_of :sun_st_time, :if =>:working_on_sunday?, :message => "Start time and end time not present for sunday"

  validate :mon_st_end_time, :tue_st_end_time, :wed_st_end_time, :thu_st_end_time, :fri_st_end_time, :sat_st_end_time, :sun_st_end_time

  private
  def mon_st_end_time
    if self.mon_st_time.present?&&self.mon_end_time.present?
      if self.mon_st_time.to_datetime<self.mon_end_time.to_datetime
        puts "---true at monday ----"             
      else
        errors.add(:mon_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:mon_end_time, "Start time should be less than end time ")
    end
  end

  def tue_st_end_time
    if self.tue_st_time.present?&&self.tue_end_time.present?
      if self.tue_st_time.to_datetime<self.tue_end_time.to_datetime
        puts "---true at tuesday----"             
      else
        errors.add(:tue_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:tue_end_time, "Start time should be less than end time ")
    end
  end

  def wed_st_end_time
    if self.wed_st_time.present? && self.wed_end_time.present?
      # puts "======= #{self.wed_st_time.to_datetime} ========= #{self.wed_end_time.to_datetime} ======="
      puts "True"
      if self.wed_st_time.to_datetime<self.wed_end_time.to_datetime
        puts "---true at wed ----"             
      else
        errors.add(:wed_st_time, "Start time should be less than end time ")
        # errors.add(:wed_end_time, "Start time should be less than end time ")
      end
    else
      puts "False"
      errors.add(:wed_end_time, "Start time and end time should be present ")  
    end
  end

  def thu_st_end_time
    if self.thu_st_time.present?&&self.thu_end_time.present?
      if self.thu_st_time.to_datetime<self.thu_end_time.to_datetime
        puts "---true at thursday ----"             
      else
        errors.add(:thu_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:thu_end_time, "Start time should be less than end time ")
    end
  end

  def fri_st_end_time
    if self.fri_st_time.present?&&self.fri_end_time.present?
      if self.fri_st_time.to_datetime<self.fri_end_time.to_datetime
        puts "---true at friday----"             
      else
        errors.add(:fri_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:fri_end_time, "Start time should be less than end time ")
    end
  end

  def sat_st_end_time
    if self.sat_st_time.present?&&self.sat_end_time.present?
      if self.sat_st_time.to_datetime<self.sat_end_time.to_datetime
        puts "---true at saturday ----"             
      else
        errors.add(:sat_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:sat_end_time, "Start time should be less than end time ")
    end
  end

  def sun_st_end_time
    if self.sun_st_time.present?&&self.sun_end_time.present?
      if self.sun_st_time.to_datetime<self.sun_end_time.to_datetime
        puts "---true at sunday ----"             
      else
        errors.add(:sun_st_time, "Start time should be less than end time ")
      end
    else
      errors.add(:sun_end_time, "Start time should be less than end time ")
    end
  end

end
