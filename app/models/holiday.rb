class Holiday < ApplicationRecord
  validates :description , :date, :uniqueness => {:case_sensitive => true}
  validates :description, :date, :presence => true
  validates :description, :length => {:maximum => 250}
end