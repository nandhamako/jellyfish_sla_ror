class GeneralSetting < ApplicationRecord
  validate :fd_api_key_is_valid
  after_save :store_field_values

  def fd_api_key_is_valid
    if freshdesk_api_key.present? && freshdesk_domain.present?
      response = HTTParty.get("#{self.format_domain}/api/v2/ticket_fields", headers: {Authorization: "Basic " + Base64.encode64("#{freshdesk_api_key}:x")})
      if ((!response.present? || !response.parsed_response.present?) || response.code!=200)
        errors.add(:freshdesk_api_key, "Freshdesk API key is invalid")
      end
    elsif !freshdesk_domain.present?
      errors.add(:freshdesk_domain, "Please fill Freshdesk domain")
    elsif !freshdesk_api_key.present?
      errors.add(:freshdesk_api_key, "Please fill Freshdesk API key")
    end
  end

  def store_field_values
    if (!saved_change_to_api_key?)
      response = HTTParty.get("#{self.format_domain}/api/v2/ticket_fields", headers: {Authorization: "Basic " + Base64.encode64("#{freshdesk_api_key}:x")})
      if (response.present? && response.parsed_response.present? || response.code==200)
        status_field = response.parsed_response.detect {|f| f["name"] == 'status'}
        priority_field = response.parsed_response.detect {|f| f["name"] == 'priority'}
        checkAndUpdateStatuses(status_field)
        checkAndUpdatePriorities(priority_field)
      end
    end
  end

  def checkAndUpdateStatuses(status_field)
    status_field["choices"].each do |key, value|
      status = Status.find_by(fd_value: key.to_i)
      if !status.present?
        Status.create(fd_value: key.to_i, name: value.first)
      end
    end
  end

  def checkAndUpdatePriorities(priority_field)
    priority_field["choices"].each do |key, value|
      priority = Priority.find_by(fd_value: value.to_i)
      if !priority.present?
        Priority.create(fd_value: value.to_i, name: key)
      end
    end
  end

  def format_domain
    return ((self.freshdesk_domain.to_s.include? "https://") ? self.freshdesk_domain : "https://#{self.freshdesk_domain}")
  end
end
