class Ticket < ApplicationRecord
  has_many :ticket_histories
  after_save :update_history

  def last_status
    Status.find_by(id: self.last_status_id)
  end

  def current_status
    Status.find_by(id: self.current_status_id)
  end

  def priority
    Priority.find_by(fd_value: self.fd_data["priority"])
  end

  def get_status(status_id)
    Status.find_by(id: status_id)
  end

  def violation_status(tp,status_id)
    @sla_settings = SlaSetting.all         
    associated_record = SlaSetting.find_by(priority_id: self.fd_data["priority"],status_id: status_id)
    # puts "Alotted hours in SLA :#{associated_record.hours}"
    # puts "Alotted minutes in SLA :#{associated_record.minutes}"
    hours_in_seconds = associated_record.hours*60*60
    minutes_in_seconds = associated_record.minutes*60
    # puts "Converted hours in seconds :#{hours_in_seconds}"
    # puts "Converted minutes in seconds :#{minutes_in_seconds}"
    puts "~"*50  
    puts "#{tp}"
    puts "~"*50  
    alotted_time = hours_in_seconds + minutes_in_seconds
    if tp < alotted_time
      return "Within SLA"
    else
      return "Oustide SLA"
    end   
  end

  def calculate_sla        
    @business_hours = BusinessHour.first
    @holidays = Holiday.all
    business_hours = @business_hours
    non_working_days = @holidays.pluck(:date)

    monday = is_working_on_monday
    tuesday = is_working_on_tuesday
    wednesday = is_working_on_wednesday
    thursday = is_working_on_thursday
    friday = is_working_on_friday
    saturday = is_working_on_saturday
    sunday = is_working_on_sunday

    get_working_configration = working_hours_object(monday,tuesday,wednesday,thursday,friday,saturday,sunday)
    puts "==========================="*5
    puts "#{get_working_configration}"
    puts "==========================="*5
    WorkingHours::Config.working_hours = get_working_configration
    WorkingHours::Config.time_zone = @business_hours.time_zone
    WorkingHours::Config.holidays = non_working_days
  end

  def update_history  
    status_changed = self.saved_changes[:current_status_id].present?   
    if status_changed
      get_ticket_history = self.ticket_histories.where(status_id: self.last_status_id).last     
      if get_ticket_history
        get_ticket_history.out_time = Time.now
        get_ticket_history.save
        from = get_ticket_history.in_time
        to = get_ticket_history.out_time
        time = WorkingHours.working_time_between(from, to)
        get_ticket_history.time_spent = time
        get_ticket_history.save
        puts "~"*50  
        puts " ==== #{time} ==== "
        puts "~"*50  
        create_history = self.ticket_histories.create(status_id: self.current_status_id, in_time: Time.now)    
      else
        create_history = self.ticket_histories.create(status_id: self.current_status_id, in_time: Time.now)    
      end
    end
  end

  def working_hours_object(monday,tuesday,wednesday,thursday,friday,saturday,sunday)
    hash = {}
    if(monday != nil)
      hash[:mon] = monday
    end
    if(tuesday != nil)
      hash[:tue] = tuesday
    end
    if(wednesday != nil)
      hash[:wed] = wednesday
    end
    if(thursday != nil)
      hash[:thu] = thursday
    end
    if(friday != nil)
      hash[:fri] = friday
    end
    if(saturday != nil)
      hash[:sat] = saturday
    end
    if(sunday != nil)
      hash[:sun] = sunday
    end
    return hash
  end

  def is_working_on_monday
    if @business_hours.working_on_monday then    
     return {@business_hours.mon_st_time => @business_hours.mon_end_time}
    else
     return nil
    end
  end

  def is_working_on_tuesday
    if @business_hours.working_on_tuesday then
     return {@business_hours.tue_st_time => @business_hours.tue_end_time}
    else
     return nil
    end
  end

  def is_working_on_wednesday
    if @business_hours.working_on_wednesday then
     return {@business_hours.wed_st_time=> @business_hours.wed_end_time}
    else
     return nil
    end
  end

  def is_working_on_thursday
    if @business_hours.working_on_thursday then
     return {@business_hours.thu_st_time => @business_hours.thu_end_time}
    else
     return nil
    end
  end

  def is_working_on_friday
    if @business_hours.working_on_friday then
     return {@business_hours.fri_st_time => @business_hours.fri_end_time}
    else
     return nil
    end
  end

  def is_working_on_saturday
    if @business_hours.working_on_saturday then
     return {@business_hours.sat_st_time => @business_hours.sat_end_time}
    else
     return nil
    end
  end

  def is_working_on_sunday
    if @business_hours.working_on_sunday then
     return {@business_hours.sun_st_time => @business_hours.sun_end_time}
    else
     return nil
    end
  end
end
