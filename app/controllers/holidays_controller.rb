class HolidaysController < ApplicationController
  def index
    @holidays = Holiday.all
  end

  def new
    @holiday = Holiday.new
  end

  def create
    @holiday = Holiday.new(holiday_settings_params)
    if @holiday.save
      redirect_to "/holidays", notice: "Holidays Saved" 
    else
      render "new"
    end
  end

  def edit 
    @holiday = Holiday.find(params[:id])   
  end

  def update
    @holiday = Holiday.find(params[:id])
    if @holiday.update(holiday_settings_params)
      redirect_to "/holidays", notice: "Holiday saved"
    else
      render "new"
    end
  end

  def destroy
    @holiday = Holiday.find(params[:id])
    @holiday.destroy
    redirect_to "/holidays", notice: "Holiday was successfully deleted"
  end

  private
  def holiday_settings_params
    params.require(:holiday).permit(:id,:date,:description)
  end  
end
