class TicketsController < ApplicationController
  before_action :authenticate_user!
  def index
    @tickets = Ticket.page(params[:page] || 1).per(20)    
  end  
end

