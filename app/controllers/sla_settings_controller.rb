class SlaSettingsController < ApplicationController
  def index    
    @sla_settings = SlaSetting.all
    @priorities = Priority.all
    @statuses = Status.all
  end

  def new
    @priority = Priority.all
    @status = Status.all
  end 
  
  def create     
    @sla_settings = SlaSetting.create(sla_settings_params)      
    @sla = @sla_settings.map {|val| val.hours.present? }
    if @sla.include? true
      redirect_to "/sla_settings", notice: "Sla Settings Saved"
    else
      redirect_to "/sla_settings/new", alert: "Invalid SLA Setting"
    end   
  end

  def edit 
    @sla_settings = SlaSetting.all
    @priorities = Priority.all
    @statuses = Status.all
  end
 
  def slaUpdate      
    puts "#{params[:sla_settings].pluck(:id)}" 
    SlaSetting.update(params[:sla_settings].pluck(:id),sla_settings_params)  
    redirect_to "/sla_settings", notice: "Sla Settings Saved"  
  end
    
  private
  def sla_settings_params
    params.require(:sla_settings).map do |p|
      p.permit(:id,:status_id, :priority_id, :hours, :minutes)      
    end
  end
  
end
