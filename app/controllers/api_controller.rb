class ApiController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate_user!, only: [:access_settings, :generate_api_key]
  before_action :verify_api_key, only: [:sync_ticket]
  def sync_ticket
    puts "inside sync ticket"
    if params[:data].present?
      checkTicketIsPresent
    end
    render :plain => {success:true}.to_json, status: 200, content_type: 'application/json'
  end

  def access_settings
    @general_settings = GeneralSetting.first
  end

  def generate_api_key
    payload = {time: Time.now.to_i, rand_num: Random.rand(9999999999999999) }
    token = JWT.encode payload, nil, 'none'
    general_settings = GeneralSetting.first
    puts token
    if(general_settings.present?)
      general_settings.update(api_key: token)
    else
      GeneralSetting.create(api_key: token)
    end
    redirect_to "/access_settings", notice: "API key generated"
  end

  private
  def checkTicketIsPresent
    puts "~"*100
    puts "inside check ticket"
    params[:data] = JSON.parse(params[:data].to_json)
    puts params[:data][:ticket][:id].inspect
    if params[:data][:ticket].present?
      tickets = Ticket.where("fd_data ->> 'id' = '#{params[:data][:ticket][:id]}'")     
      tickets.present? ? updateTicket(tickets.first) : createTicket
    end
  end

  def createTicket
    status = getTicketStatus
    ticket = Ticket.create(fd_data: params[:data][:ticket], current_status_id: status.try(:id), last_status_id: status.try(:id))    
    ticket.calculate_sla
  end

  def updateTicket(ticket)
    status = getTicketStatus
    if (ticket.current_status_id != status.try(:id))
      ticket.update(last_status_id: ticket.current_status_id, current_status_id: status.id, fd_data: params[:data][:ticket])     
      ticket.calculate_sla
    else
      ticket.update(fd_data: params[:data][:ticket])
      ticket.calculate_sla
    end    
    ticket.calculate_sla
  end

  # def fetchStatusAndPriorities
  #   # Base64.encode64
  #   response = HTTParty.get("https://makoitlab.freshdesk.com/api/v2/ticket_fields", headers: {Authorization: "Basic " + Base64.encode64('zic7Prl6hlO18uv7qDgB:x')})
  #   if response.present? && response.parsed_response.present?
  #     status_field = response.parsed_response.detect {|f| f["name"] == 'status'}
  #     priority_field = response.parsed_response.detect {|f| f["name"] == 'priority'}
  #     checkAndUpdateStatuses(status_field)
  #     checkAndUpdatePriorities(priority_field)
  #     checkTicketIsPresent
  #   end
  # end

  # def checkAndUpdateStatuses(status_field)
  #   status_field["choices"].each do |key, value|
  #     status = Status.find_by(fd_value: key.to_i)
  #     if !status.present?
  #       Status.create(fd_value: key.to_i, name: value.first)
  #     end
  #   end
  # end

  # def checkAndUpdatePriorities(priority_field)
  #   priority_field["choices"].each do |key, value|
  #     priority = Priority.find_by(fd_value: value.to_i)
  #     if !priority.present?
  #       Priority.create(fd_value: value.to_i, name: key)
  #     end
  #   end
  # end

  def getTicketStatus
    return Status.find_by(fd_value: params[:data][:ticket][:status].to_i)
  end

  def verify_api_key
    if(params[:iparams].present?)
      data = JSON.parse(params[:iparams].to_json)
      puts "~"*100
      puts params[:iparams]
      api_key = GeneralSetting.try(:first).try(:api_key)
      puts "~"*100
      puts data["middleware_apiKey"]
      puts api_key
      puts "~"*100
      if(data["middleware_apiKey"]!=api_key)
        render :plain => {success:false}.to_json, status: 401, content_type: 'application/json'
      end
    else
      render :plain => {success:false}.to_json, status: 401, content_type: 'application/json'
    end
  end
end
