class BusinessHoursController < ApplicationController
  
  def index       
    @business_hours = BusinessHour.first
  end

  def new
    @identifiers = TZInfo::Timezone.all_identifiers
    @business_hours = BusinessHour.new
  end

  def create
    @identifiers = TZInfo::Timezone.all_identifiers
    @business_hours = BusinessHour.new(business_hours_params)     
    if @business_hours.save
      redirect_to "/business_hours", notice:"Business Hours Saved"
    else
      render "new"
    end
  end

  def edit
    @identifiers = TZInfo::Timezone.all_identifiers
    @business_hours = BusinessHour.find(params[:id])
  end

  def update
    @identifiers = TZInfo::Timezone.all_identifiers
    @business_hours = BusinessHour.find(params[:id])
    if @business_hours.update(business_hours_params)
      redirect_to "/business_hours", notice:"Business Hours Saved"
    else
      render "edit"
    end
  end

  private
    def business_hours_params
      params.require(:business_hour).permit(:working_on_monday,:mon_st_time,:mon_end_time,:working_on_tuesday,:tue_st_time,:tue_end_time,:working_on_wednesday,:wed_st_time,:wed_end_time,:working_on_thursday,:thu_st_time,:thu_end_time,:working_on_friday,:fri_st_time, :fri_end_time,:working_on_saturday,:sat_st_time,:sat_end_time,:working_on_sunday,:sun_st_time,:sun_end_time,:time_zone)
    end
end
