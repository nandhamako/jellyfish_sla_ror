Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions', passwords: 'users/passwords' }
  resources :api
  root "general_settings#index"
  post '/api/sync_ticket', to: "api#sync_ticket"
  get '/access_settings', to: "api#access_settings"
  post '/generate/api_key', to: "api#generate_api_key"
  post '/upate_sla_settings', to: "sla_settings#slaUpdate"

  resources :general_settings
  resources :tickets
  resources :user_settings
  resources :sla_settings
  resources :business_hours
  resources :holidays
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
